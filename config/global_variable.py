NETWORK_IP = '123.123.123.123'

# Blynk setting
BLYNK_IP = '192.168.1.1'
BLYNK_PORT = 8080
BLYNK_AUTH = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

# pin dict
PIN = {"terminal": 1,
       # Curtain
       "curtain": 5,
       # Motion sensor
       "ms_illumination": 6,
       "ms_activity": 7,
       "ms_battery": 8,
       # Light switch
       "living_room": 10,
       "dining_room": 11,
       "kitchen": 12,
       "stove": 13,
       "corridor": 14,
       "toilet": 15,
       "terrace": 16,
       "small_room": 17,
       "bedroom": 18,
       # Gateway
       "gw_illumination": 20,
       "gw_color": 21,
       "gw_led": 22,
       # Environment
       "temperature": 31,
       "humidity": 32,
       "pm2.5": 33,
       "pm10": 34,
       }


# Sony tv IP & pin code
TV_IP = '192.168.1.2'
TV_PIN_CODE = '5219'

# Dyson fan account
DYSON_ACC = "my_email@mail.com"
DYSON_PW = "my_password"
DYSON_LOC = "GB"

# OpenHab setting
OPENHAB_IP = '192.168.1.3'
OPENHAB_PORT = '8080'
GATEWAY_ID = "xxxxxxxxxxxxxxx"
# Light switch control dict
LIGHT_ID_DICT = {"small_room":  "mihome_ctrl_neutral1_xxxxxxxxxxxxxxx_ch1",
                 "terrace":     "mihome_ctrl_neutral1_xxxxxxxxxxxxxxx_ch1",
                 "bedroom":     "mihome_ctrl_neutral1_xxxxxxxxxxxxxxx_ch1",
                 "dining_room": "mihome_ctrl_neutral2_xxxxxxxxxxxxxxx_ch1",
                 "living_room": "mihome_ctrl_neutral2_xxxxxxxxxxxxxxx_ch2",
                 "kitchen":     "mihome_ctrl_neutral2_xxxxxxxxxxxxxxx_ch2",
                 "stove":       "mihome_ctrl_neutral2_xxxxxxxxxxxxxxx_ch1",
                 "toilet":      "mihome_ctrl_neutral2_xxxxxxxxxxxxxxx_ch2",
                 "corridor":    "mihome_ctrl_neutral2_xxxxxxxxxxxxxxx_ch1",
                 }
CURTAIN_ID = "xxxxxxxxxxxxxxx"
MOTION_SENSOR_ID = "xxxxxxxxxxxxxxx"
