from openhab import OpenHAB

from config.global_variable import *


base_url = f'http://{OPENHAB_IP}:{OPENHAB_PORT}/rest'
openhab = OpenHAB(base_url)


def light_switch(light, action=None):
    if light in LIGHT_ID_DICT:
        item = openhab.get_item(LIGHT_ID_DICT[light])
        if action is None:
            return True if item.state == "ON" else False
        elif action:
            item.on()
            return True
        else:
            item.off()
            return False


def curtain_control(action=None):
    item = openhab.get_item(f'mihome_curtain_{CURTAIN_ID}_curtainControl')
    if action is None:
        return item.state
    else:
        item.command(action)
        return action


def set_gateway_color(c):
    c = (c['l'] << 24) + (c['r'] << 16) + (c['g'] << 8) + (c['b'])
    openhab.get_item(f"mihome_basic_{GATEWAY_ID}_writeMessage").command(f'"rgb":{c}')


def get_sensor():
    illumination_item = openhab.get_item(f'mihome_sensor_motion_aq2_{MOTION_SENSOR_ID}_illumination')
    activity_item = openhab.get_item(f'mihome_sensor_motion_aq2_{MOTION_SENSOR_ID}_lastMotion')
    battery_item = openhab.get_item(f'mihome_sensor_motion_aq2_{MOTION_SENSOR_ID}_batteryLevel')
    return {'illumination': illumination_item.state,
            'activity': activity_item.state.replace(tzinfo=None),
            'battery': battery_item.state}
