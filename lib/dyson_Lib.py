import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from libpurecool.dyson import DysonAccount
from libpurecool.const import *
from libpurecool.dyson_pure_state import DysonPureHotCoolState, DysonPureCoolState, DysonEnvironmentalSensorState

from config.global_variable import DYSON_ACC, DYSON_PW, DYSON_LOC

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class DysonFan():
    def __init__(self):
        # Log to Dyson account
        print("Try connect to dyson server")
        self.dyson_account = DysonAccount(DYSON_ACC, DYSON_PW, DYSON_LOC)
        self.logged = self.dyson_account.login()
        if not self.logged:
            raise Exception('Unable to login to Dyson account')
        print("Connect success, getting devices")
        # List devices available on the Dyson account
        self.devices = self.dyson_account.devices()
        self.fan = self.devices[0]
        self.fan.auto_connect()
        print("Get device success!")
        #self.fan.add_message_listener(DysonFan.on_message)

    def get_environment(self):
        environment = {"humidity": self.fan.environmental_state.humidity,
                       "temperature": round(self.fan.environmental_state.temperature - 273.15, 1),
                       "voc": self.fan.environmental_state.volatile_organic_compounds,
                       "no": self.fan.environmental_state.nitrogen_dioxide,
                       "pm2.5": self.fan.environmental_state.particulate_matter_25,
                       "pm10": self.fan.environmental_state.particulate_matter_10}
        return environment

    def set_fan_speed(self, speed):
        if speed == "AUTO":
            self.fan.set_configuration(auto_mode=AutoMode.AUTO_ON, fan_speed=FanSpeed.FAN_SPEED_AUTO)
            return
        if speed == 1:
            fan_speed = FanSpeed.FAN_SPEED_1
        elif speed == 2:
            fan_speed = FanSpeed.FAN_SPEED_2
        elif speed == 3:
            fan_speed = FanSpeed.FAN_SPEED_3
        elif speed == 4:
            fan_speed = FanSpeed.FAN_SPEED_4
        elif speed == 5:
            fan_speed = FanSpeed.FAN_SPEED_5
        elif speed == 6:
            fan_speed = FanSpeed.FAN_SPEED_6
        elif speed == 7:
            fan_speed = FanSpeed.FAN_SPEED_7
        elif speed == 8:
            fan_speed = FanSpeed.FAN_SPEED_8
        elif speed == 9:
            fan_speed = FanSpeed.FAN_SPEED_9
        elif speed == 10:
            fan_speed = FanSpeed.FAN_SPEED_10
        else:
            return
        self.fan.set_configuration(fan_speed=fan_speed)

    def set_fan_power(self, action):
        if action:
            self.fan.turn_on()
        else:
            self.fan.turn_off()

    def set_oscillation(self, action, low=150, high=200):
        if action:
            self.fan.enable_oscillation(oscillation_angle_low=low, oscillation_angle_high=high)
        else:
            self.fan.disable_oscillation()

    def set_night_mode(self, action):
        if action:
            self.fan.enable_night_mode()
        else:
            self.fan.disable_night_mode()

    def set_front_direction(self, action):
        if action:
            self.fan.enable_frontal_direction()
        else:
            self.fan.disable_frontal_direction()

    def set_heat_mode(self, action):
        if action:
            self.fan.enable_heat_mode()
        else:
            self.fan.disable_heat_mode()

    def set_heat_target(self, celsius):
        self.fan.set_configuration(heat_target=HeatTarget.celsius(celsius))

    @staticmethod
    def on_message(msg):
        # Message received
        if isinstance(msg, DysonPureCoolState):
            # Will be true for DysonPureHotCoolState too.
            print("DysonPureCoolState message received")
        if isinstance(msg, DysonPureHotCoolState):
            print("DysonPureHotCoolState message received")
        if isinstance(msg, DysonEnvironmentalSensorState):
            print("DysonEnvironmentalSensorState received")
        print(msg)

# tilt=TILT
# fan 橫左






