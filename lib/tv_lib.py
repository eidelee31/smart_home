# https://github.com/dcnielsen90/python-bravia-tv
from bravia_tv import BraviaRC
from config.global_variable import TV_IP, TV_PIN_CODE


class BraviaTV():
    def __init__(self):
        self.tv = BraviaRC(TV_IP)
        if not self.tv.connect(TV_PIN_CODE, 'my_device_id', 'my device name'):
            raise Exception("Connect TV error!")
