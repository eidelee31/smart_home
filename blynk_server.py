import time
import datetime as dt
import threading

import blynklib
import blynktimer

from lib.tv_lib import *
from lib.dyson_Lib import *
from lib.openhab_lib import *
from config.global_variable import *


def now():
    return dt.datetime.now().strftime('%H:%M:%S ')


R_PIN = {v: k for k, v in PIN.items()}
gw_color = {"r": 0, "g": 0, "b": 0, 'l': 0}
light_status = {k: False for k in LIGHT_ID_DICT.keys()}


# Initialize Blynk
blynk = blynklib.Blynk(BLYNK_AUTH, server=BLYNK_IP, port=BLYNK_PORT)
fan = DysonFan()
# create timers dispatcher instance
timer = blynktimer.Timer()


def print_terminal(msg):
    blynk.virtual_write(PIN['terminal'], now() + str(msg) + "\n")
    print(now(), msg)


@blynk.handle_event("connect")
def connect_handler():
    print_terminal('[CONNECT_EVENT]')


@blynk.handle_event("disconnect")
def disconnect_handler():
    print_terminal('[DISCONNECT_EVENT]')


@blynk.handle_event('write V*')
def write_virtual_pin_handler(pin, value):
    print_terminal(f"Pin: {pin} Value: {value}")
    # Terminal
    if pin == PIN['terminal']:
        print_terminal(eval(value[0]))
    # Light switch
    elif R_PIN[pin] in LIGHT_ID_DICT:
        light_status[R_PIN[pin]] = light_switch(R_PIN[pin], True if value[0] == '1' else False)
    # Curtain control
    elif pin == PIN['curtain']:
        curtain_control(int(value[0]))
    # Gateway control
    elif pin == PIN['gw_illumination']:
        gw_color['l'] = int(value[0])
        set_gateway_color(gw_color)
        blynk.virtual_write(PIN['gw_led'], int(255 / 100 * gw_color['l']))
    elif pin == PIN['gw_color']:
        gw_color["r"], gw_color["g"], gw_color["b"] = int(value[0]), int(value[1]), int(value[2])
        set_gateway_color(gw_color)
        color = '#%02x%02x%02x' %(gw_color["r"], gw_color["g"], gw_color["b"])
        blynk.set_property(PIN['gw_led'], "color", color)
        blynk.set_property(PIN['gw_illumination'], "color", color)


@blynk.handle_event('internal_acon')
def app_connect_handler(*args):
    print_terminal('[APP_CONNECT_EVENT]')


@blynk.handle_event('internal_adis')
def app_disconnect_handler(*args):
    print_terminal('[APP_DISCONNECT_EVENT]')


@timer.register(interval=1, run_once=False)
def blynk_timer():
    # Update environment values
    env = fan.get_environment()
    blynk.virtual_write(PIN['temperature'], env['temperature'])
    blynk.virtual_write(PIN['humidity'], env['humidity'])
    blynk.virtual_write(PIN['pm2.5'], env['pm2.5'])
    blynk.virtual_write(PIN['pm10'], env['pm10'])

    # Update motion sensor
    ms_status = get_sensor()
    blynk.virtual_write(PIN['ms_illumination'], ms_status['illumination'])
    blynk.virtual_write(PIN['ms_activity'], ms_status['activity'])
    blynk.virtual_write(PIN['ms_battery'], ms_status['battery'])

    # Auto on toilet light
    last_activity = (dt.datetime.now() - ms_status['activity']).total_seconds()
    if last_activity <= 3 and not light_status['toilet']:
        light_status['toilet'] = light_switch('toilet', True)
        blynk.virtual_write(PIN['toilet'], 1)
    elif last_activity >= 60 * 60 and light_status['toilet']:
        light_status['toilet'] = light_switch('toilet', False)
        blynk.virtual_write(PIN['toilet'], 0)

    # Update light status
    for key, value in light_status.items():
        status = light_switch(key)
        if value != status:
            light_status[key] = status
            print_terminal(f"{key}: {status}")
            blynk.virtual_write(PIN[key], 1 if status else 0)


###########################################################
# infinite loop that waits for event
###########################################################

while True:
    blynk.run()
    timer.run()
